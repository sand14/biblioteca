#include <iostream>
using namespace std;

#include "author.h"

author::author(void)
{
	firstname = " ";
	lastname = " ";
}

author::~author(void)
{
}

string author::getLastName()const
{
	return lastname;
}

string author::getFirstName() const
{
	return firstname;
}

void author::setName(const string& newLastName, const string& newFirstName)
{
	firstname = newFirstName;
	lastname = newLastName;
}

void author::printName() const {
	cout << lastname << " " << firstname << " ";
}