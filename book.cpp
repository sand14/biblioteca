#include "book.h"

Book::Book()
{
	bookID = "";
	title = "";
	author = "";
	publisher = "";
	year = 0;
	status = 1;
}

void Book::setBook(const string& newBookID, const string& newTitle, const string& newAuthor, const string& newPublisher, int newYear, int newStatus)
{
	bookID = newBookID;
	title = newTitle;
	author = newAuthor;
	publisher = newPublisher;
	year = newYear;
	status = newStatus;
}

void Book::addNewBook(istream& ins)
{
	cout << "Name: ";
	getline(ins, title);

	cout << "Enter Id Number ";
	getline(ins, bookID);

	cout << "Enter author: ";

	getline(ins, author);

	cout << "Enter Publisher: ";
	getline(ins, publisher);

	cout << "Enter Publication Year: ";
	ins >> year;


}
string Book::getBookID() const
{
	return bookID;
}

string Book::getTitle() const
{
	return title;
}

string Book::getAuthor() const
{
	return author;
}
string Book::getPublisher() const
{
	return publisher;
}


int Book::getYear()const
{
	return year;
}

void Book::setBookID(const string& newID)
{
	bookID = newID;
}

void Book::setTitle(const string& newTitle)
{
	title = newTitle;
}

void Book::setAuthor(const string& newAuthor)
{
	author = newAuthor;
}

void Book::setPublisher(const string& newPublisher)
{
	publisher = newPublisher;
}


void Book::setYear(int newYear)
{
	year = newYear;
}

void Book::setStatus(int newStatus)
{
	status = newStatus;
}

void Book::printBook() const
{
	cout << "Book ID: " << bookID << endl;
	cout << "Title: " << title << endl;
	cout << "Author: " << author << endl;
	cout << "Publisher: " << publisher << endl;
	cout << "Published: " << year << endl;
	cout << "Available ";
	if (status == 1)
		cout << "Yes";
	else
		cout << "No";

}

void Book::printBookToFile(ofstream& out)
{
	out << bookID << " " << title << endl;
	out << author << endl;
	out << publisher << endl;
	out << year << " " << status;
	out << endl;
}
Book::~Book()
{

}

istream& operator >> (istream& ins, Book& tempBook)
{
	tempBook.addNewBook(ins);
	return ins;
}




