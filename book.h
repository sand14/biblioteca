#ifndef BOOK_H
#define BOOK_H
#include <string>
#include <iostream>
#include <fstream>
using namespace std;


class Book
{
	friend istream& operator>>(istream& ins, Book& tempBook);
public:
	Book();
	void setBook(const string& newBookID, const string& newTitle, const string& newAuthor, const string& newPublisher,int newYear,int newStatus);
	void addNewBook(istream& ins);
	string getBookID() const;
	string getTitle() const;
	string getAuthor() const;
	string getPublisher() const;
	int getYear() const;
	void setBookID(const string& newID);
	void setTitle(const string& newTitle);
	void setPublisher(const string& newPublisher);
	void setAuthor(const string& newAuthor);
	void setYear(int newYear);
	void setStatus(int newStatus);
	void printBook() const;
	void printBookToFile(ofstream& out);
	~Book();
private:
	string bookID, title, publisher, author;
	int year, status;
};

#endif