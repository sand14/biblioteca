#pragma once

#include "book.h"

class bookstore {
public:
	bookstore(void);
	virtual ~bookstore(void);
	virtual void read() = 0;
	virtual void print() = 0;
	virtual char* getTitle() = 0;
	virtual char* getAuthor() = 0;
	virtual char* getEditor() = 0;
	virtual int getYear() = 0;
	virtual int getActive() = 0;
};