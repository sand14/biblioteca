#pragma once

#include "book.h";
#include <fstream>

using namespace std;


class database   {
private:
	book bookarray[100];
public:
	void readbooks(int nr,ifstream db) {
		db.open("maindb.txt");
		db >> nr;
		db.close();
		for (int i = 0; i < nr; i++)
		{
			bookarray[i].readfromfile(db);
		}
	}

	void showbooks(int nr) {
		for (int i = 0; i < nr; i++) {
			bookarray[i].print();
		}
	}
};