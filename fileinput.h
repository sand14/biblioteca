#include "book.h"
#include "bookdatabase.h"

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

const char db[] = "maindb.txt";

void createBookList(ifstream& infile, BookDatabase& Database)
{


	Book tempBook;
	string bookID, title, author, publisher;
	int year,status;

	infile >> bookID;

	while (bookID != "END")
	{
		if (infile.peek() == ' ')
			infile.ignore();
		getline(infile, title);
		getline(infile, author);
		getline(infile, publisher);


		infile >> year;
		infile >> status;


		tempBook.setBook(bookID, title, author, publisher, year, status);
		Database.addBook(tempBook);

		infile >> bookID;
	}
}

void readBookData(BookDatabase& Database)
{
	ifstream infile;

	infile.open(db);

	if (!infile)
	{
		cerr << "Input file does not exist." << endl;
		system("Pause");
		exit(1);
	}

	createBookList(infile, Database);

	infile.close();
}
#pragma once
