#include <iostream>

#include "password.h"
#include "bookdatabase.h"
#include "fileinput.h"

using namespace std;

void displayMenu();
void processChoice(BookDatabase& database);

int main() {
	char password[80], my_password[20];
	int in, no_attempt, ok;
	char modif;
	FILE* pass;

	fopen_s(&pass, "PASS.TXT", "r");
	if (pass != NULL) {
		fscanf_s(pass, "%s", my_password, 20);
		decode_pass(my_password);
		fclose(pass);
	}
	else
	{
		cout << "\nError opening file";
		cin.get();
		exit(0);
	}
	no_attempt = 0;
	ok = 0;
	do {
		system("cls");
		cout << "Enter pasword: ";
		read_pass(password);
		if (strcmp(password, my_password) == 0)
		{
			ok = 1;
			break;
		}
		no_attempt++;
	} while (no_attempt < 3);
	if (ok == 0)
	{
		cout << "\n\n INCORRECT PASSWORD !";
		cin.get();
		return 0;
	}

	cout << "\n ============= Password correct, Welcome ! ====================\n";
	
	BookDatabase Database;
	readBookData(Database);
	displayMenu();
	processChoice(Database);
	cout << endl;

	system("pause");

	return 0;
}


void displayMenu() {
		cout << "============= Database control panel ============"<<endl;
		cout << "Select one of the following:" <<endl;
		cout << "    1: Add a new book" << endl;
		cout << "    2: Print all books" << endl;
		cout << "    3: Edit a book" << endl;
		cout << "    4. Save to File" << endl;
		cout << "    0: Exit" << endl;
}

void processChoice(BookDatabase& database) {
	string stringEdit = " ";
	int intEdit, choice;
	Book book1;
	ofstream out;
	out.open("maindb.txt");
	cout << "\nEnter your choice: ";
	cin >> choice;
	cout << endl;

while (choice != 0) {
	switch (choice)
	{
	case 1:
		cout << "1. Add a book" << endl;
		cin.ignore(1, ' ');
		cin >> book1;
		database.addBook(book1);

		break;
	case 2:
		database.printall();

		break;

	case 3:
		database.printBookTitle();
		cout << "Enter index to edit? ";
		cin >> intEdit;
		database.editBookAt(--intEdit);

		break;

	case 4:
		database.saveToFile(out);

		break;
	default:
		cout << "Invalid Selection" << endl;

		break;
	}

	system("Pause");
	system("CLS");
	displayMenu();

	cout << "\nEnter your choice: ";
	cin >> choice;
	cout << endl;
}
}
