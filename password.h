#pragma once

#include <stdio.h>
#include <conio.h>
#include <string>


void read_pass(char* password)
{
	char ch;
	int i = 0;
	do
	{
		ch = _getch();
		password[i] = ch;
		if (ch != 27 && ch != 13)	
			_putch('*');
		else
			break;
		i++;
	} while (i < 19);
	password[i] = '\0';
}

void encode_pass(char* password)
{
	for (int i = 0; password[i] != 0; i++)
		password[i] += i;
}

void decode_pass(char* password)
{
	for (int i = 0; password[i] != 0; i++)
		password[i] -= i;
}

void change_pass(char* password)
{
	FILE* fPass;
	char verif_pass[20];
	printf("\nEnter new password:");
	read_pass(password);
	printf("\nEnter password again:");

	read_pass(verif_pass);

	if (strcmp(password, verif_pass) == 0)
	{
		fopen_s(&fPass, "PASS.TXT", "w");
		if (fPass != NULL)
		{
			encode_pass(password);
			fprintf(fPass, "%s", password);
			fclose(fPass);
		}
		else
			printf("\nError - saving password !");
	}
}
